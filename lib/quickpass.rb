require 'quickpass/version'
require 'quickpass/config'
require 'quickpass/options'

require 'gpgme'
require 'clipboard'
require 'json'
require 'io/console'

# Main module logic for Quickpass
module Quickpass
  class Error < StandardError; end

  class << self
    attr_accessor :config
    attr_accessor :crypto
  end

  self.config = Quickpass.load_config
  self.crypto = GPGME::Crypto.new armor: true

  # rubocop:disable Metrics/AbcSize
  def self.put(options)
    namespace = options[:namespace]
    key = options[:key]

    data = _get
    data[namespace] ||= {}
    data[namespace][key] = prompt_password

    crypto.encrypt data.to_json, recipients: config[:key_id], output: File.open(config[:database_file], 'w+')

    puts "Added '#{key}' to '#{namespace}'"
  end
  # rubocop:enable Metrics/AbcSize

  def self.get(options)
    data = _get
    keys = data[options[:namespace]]
    password = keys[options[:key]]

    Clipboard.copy password
    puts "Clipboard will clear in 20 seconds, ctrl+c to abort"
    sleep 20
    Clipboard.copy ""
    nil
  end

  def self.list(options)
    data = _get
    namespace = options[:namespace]
    keys = data[namespace]

    puts "Namespace: '#{namespace}'"
    keys.each_key do |key|
      puts "   #{key}"
    end
  end

  def self.list_namespaces(_options)
    puts 'Namespaces:'
    _get.each_key do |key|
      puts "   #{key}"
    end
  end

  private_class_method

  def self._prompt_password(prompt)
    print prompt
    STDIN.noecho(&:gets).chomp
  end

  def self.prompt_password
    password = _prompt_password('Please enter the new password: ')
    puts
    password_confirmed = _prompt_password('Please reenter to confirm: ')
    puts

    unless password == password_confirmed
      puts "Passwords didn't match!"
      exit(-1)
    end

    password
  end

  def self._get
    return {} unless File.exist? config[:database_file]

    JSON.parse crypto.decrypt(File.open(config[:database_file], 'r')).to_s
  end
end

options = Quickpass.parse_options ARGV
Quickpass.send options[:action], options
