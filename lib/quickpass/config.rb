require 'psych'

# Configuration for the Quickpass module
module Quickpass
  def self.load_config
    default_options = {
      key_id: 'Key id goes here',
      database_file: "#{ENV['HOME']}/.quickpass.db"
    }

    filename = "#{ENV['HOME']}/.quickpass.yaml"

    unless File.exist? filename
      File.open(filename, 'w') do |f|
        f.write(Psych.dump(default_options))
      end
    end

    Psych.load_file(filename)
  end
end
