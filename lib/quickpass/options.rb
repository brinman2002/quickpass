require 'optparse'

# CLI option parsing
module Quickpass
  # rubocop:disable Metrics/MethodLength
  def self.parse_options(args)
    options = {
      action: :get,
      namespace: 'DEFAULT',
      key: 'login'
    }

    OptionParser.new(args) do |opts|
      opts.banner = 'Usage: quickpass [options]'

      opts.on('-g', '--get', 'Get a password (default)') do |_v|
        options[:action] = :get
      end

      opts.on('-p', '--put', 'Put a password') do |_v|
        options[:action] = :put
      end

      opts.on('-l', '--list', 'List keys in a namespace') do |_v|
        options[:action] = :list
      end

      opts.on('-t', '--list-namespaces', 'List namespaces') do |_v|
        options[:action] = :list_namespaces
      end

      opts.on('-k', '--key KEY', 'Key to operate on') do |v|
        options[:key] = v
      end

      opts.on('-n', '--namespace NAMESPACE', "Namespace of key. Default is 'DEFAULT'.") do |v|
        options[:namespace] = v
      end
    end.parse!

    options
  end
  # rubocop:enable Metrics/MethodLength
end
